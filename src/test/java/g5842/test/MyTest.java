package g5842.test;
import org.testng.annotations.Test;
import ru.tusur.kcup.g584_2.liposapr.javaunitbase.Rectangle;

import static org.testng.AssertJUnit.assertEquals;

public class MyTest {
    @Test
    public void basicTest() {
        Rectangle rect = new Rectangle();
        assertEquals(rect.getArea(), 1, 0.0001);
    }
    @Test
    public void nonIntTest() {
        Rectangle rect = new Rectangle(7.0091, 3.45);
        assertEquals(rect.getArea(), 24.181395, 1e-10);
        assertEquals(rect.getPerimeter(), 20.9182, 1e-5);
    }

    @Test
    public void setterTest() {
        Rectangle rect = new Rectangle();
        rect.setHeight(5.5);
        assertEquals(rect.getHeight(), 5.5, 0.001);
        assertEquals(rect.getArea(), 5.5, 0.001);
        assertEquals(rect.getPerimeter(), 13, 0.001);
        rect.setWidth(4);
        assertEquals(rect.getWidth(), 4, 0.001);
        assertEquals(rect.getArea(), 22, 0.001);
        assertEquals(rect.getPerimeter(), 19, 0.001);
    }
}
